package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;

public class Curso implements Serializable {

    private static final long serialVersionUID = 997084310847650620L;

    private AreaCientifica areaCientifica;
    private Long id;
    private String nome;

    public Curso(AreaCientifica areaCientifica) {
        this.areaCientifica = areaCientifica;
        this.areaCientifica.adicionarCurso(this);
    }

    // region - getters
    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public AreaCientifica getAreaCientifica() {
        return this.areaCientifica;
    }
    // endregion
}
