package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AreaCientifica implements Serializable {

    private static final long serialVersionUID = 997084310847650620L;

    private List<Curso> cursos = new ArrayList<>();
    private Long id;
    private String nome;

    public AreaCientifica() {
        super();
    }

    // region - getters
    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Curso> getCursos() {
        return this.cursos;
    }
    // endregion

    // region - function
    public void adicionarCurso(Curso curso) {
        this.cursos.add(curso);
    }

    public boolean possuiCurso(Curso curso) {
        return this.cursos.contains(curso);
    }
    // endregion
}
