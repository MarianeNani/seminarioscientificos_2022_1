package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao implements Serializable {

    private static final long serialVersionUID = 997084310847650620L;

    private Estudante estudante;
    private SituacaoInscricaoEnum situacao;
    private Seminario seminario;
    private Long id;
    private Boolean direitoMaterial;
    private LocalDateTime dataCriacao;
    private LocalDateTime dataCompra;
    private LocalDateTime dataCheckIn;

    public Inscricao(Seminario seminario) {
        this.dataCriacao = LocalDateTime.now();
        this.seminario = seminario;
        this.seminario.adicionarInscricao(this);
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
    }

    // region - getters
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    public void setDataCriacao(LocalDateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public LocalDateTime getDataCompra() {
        return this.dataCompra;
    }

    public void setDataCompra(LocalDateTime dataCompra) {
        this.dataCompra = dataCompra;
    }

    public LocalDateTime getDataCheckIn() {
        return this.dataCheckIn;
    }

    public void setDataCheckIn(LocalDateTime dataCheckIn) {
        this.dataCheckIn = dataCheckIn;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }
    // endregion

    // region - function
    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.dataCompra = LocalDateTime.now();
        this.estudante = estudante;
        this.direitoMaterial = direitoMaterial;
        estudante.adicionarInscricao(this);
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
    }

    public void cancelarCompra() {
        this.estudante.removerInscricao(this);
        this.dataCompra = null;
        this.estudante = null;
        this.direitoMaterial = null;
        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;
    }

    public void realizarCheckIn() {
        this.dataCheckIn = LocalDateTime.now();
        this.situacao = SituacaoInscricaoEnum.CHECKIN;
    }
    // endregion

}