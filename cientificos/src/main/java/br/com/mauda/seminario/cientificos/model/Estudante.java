package br.com.mauda.seminario.cientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Estudante implements Serializable {

    private static final long serialVersionUID = 997084310847650620L;

    private List<Inscricao> inscricoes = new ArrayList<>();
    private Instituicao instituicao;
    private Long id;
    private String nome;
    private String telefone;
    private String email;

    public Estudante(Instituicao instituicao) {
        this.instituicao = instituicao;
    }

    // region - getters
    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }
    // endregion

    // region - function
    public void adicionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public boolean possuiInscricao(Inscricao inscricao) {
        return this.inscricoes.contains(inscricao);
    }

    public void removerInscricao(Inscricao inscricao) {
        this.inscricoes.remove(inscricao);
    }
    // endregion
}
