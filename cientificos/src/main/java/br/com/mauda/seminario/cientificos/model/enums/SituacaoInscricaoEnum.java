package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {

    DISPONIVEL(1L, "Disponivel"),
    COMPRADO(2L, "Comprado"),
    CHECKIN(3L, "Checkin");

    private Long id;
    private String nome;

    SituacaoInscricaoEnum(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

}
